[bar/example]
width = 100%
height = 27
radius = 0
fixed-center = false

background = #1d1f21
foreground = #c5c8c6

line-size = 3
line-color =

border-size = 0
border-color =

padding-left = 0
padding-right = 0.5

module-margin-left = 1
module-margin-right = 1

font-0 = "Source Code Pro Semibold:pixelsize=10;1"
font-1 = "Font Awesome 5 Free:style=Solid:pixelsize=10;1"
font-2 = "Font Awesome 5 Brands:pixelsize=10;1"

modules-left = i3
modules-center = date
modules-right = info-wmname networkspeedupeth networkspeedup updates-arch-combined battery-combined-udev pulseaudio-tail wlan eth powermenu

tray-position =
;tray-padding =

cursor-click = pointer
cursor-scroll = ns-resize

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

label-mode-padding = 2
label-mode-foreground = #c5c8c6
label-mode-background = #282a2e

label-focused = %index%
label-focused-background = #373b41
label-focused-foreground = #6b7443
label-focused-padding = 2

label-unfocused = %index%
label-unfocused-background = #282a2e
label-unfocused-foreground = #c5c8c6
label-unfocused-padding = 2

label-visible = %index%
label-visible-background = #282a2e
label-visible-foreground = #c5c8c6
label-visible-padding = 2

label-urgent = %index%
label-urgent-background = #BA2922
label-urgent-padding = 2

[module/wlan]
type = internal/network
interface = wlp2s0
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-foreground = #c5c8c6
format-connected-background = #282a2e
format-connected-padding = 2
label-connected = %essid%

format-disconnected =

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-foreground = #c5c8c6

[module/eth]
type = internal/network
interface = enp0s3
interval = 3.0

format-connected-padding = 2
format-connected-foreground = #c5c8c6
format-connected-background = #282a2e
format-connected-prefix = " "
format-connected-prefix-foreground = #c5c8c6
label-connected = %local_ip%

format-disconnected =

[module/date]
type = internal/date
interval = 5

date =
date-alt = " %Y-%m-%d"

time = %H:%M
time-alt = %H:%M:%S

format-prefix = 
format-foreground = #c5c8c6
format-background = #282a2e
format-padding = 2

label = %date% %time%

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open-foreground = #c5c8c6
label-open-background = #282a2e
label-open-padding = 2
label-open =  Power
label-close =  cancel
label-close-foreground = #c5c8c6
label-separator = |
label-separator-foreground = #c5c8c6

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = sudo reboot

menu-2-0 = power off
menu-2-0-exec = sudo poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

[settings]
screenchange-reload = true

[global/wm]
margin-top = 0
margin-bottom = 0

[module/volume]
type = internal/volume

; Soundcard to be used
; Usually in the format hw:#
; You can find the different card numbers in `/proc/asound/cards`
master-soundcard = default
speaker-soundcard = default
headphone-soundcard = default

; Name of the master mixer
; Default: Master
master-mixer = Master

; Optionally define speaker and headphone mixers
; Use the following command to list available mixer controls:
; $ amixer scontrols | sed -nr "s/.*'([[:alnum:]]+)'.*/\1/p"
; If speaker or headphone-soundcard isn't the default, 
; use `amixer -c #` where # is the number of the speaker or headphone soundcard
; Default: none
speaker-mixer = Speaker
; Default: none
headphone-mixer = Headphone

; NOTE: This is required if headphone_mixer is defined
; Use the following command to list available device controls
; $ amixer controls | sed -r "/CARD/\!d; s/.*=([0-9]+).*name='([^']+)'.*/printf '%3.0f: %s\n' '\1' '\2'/e" | sort
; You may also need to use `amixer -c #` as above for the mixer names
; Default: none
headphone-id = 9

; Use volume mapping (similar to amixer -M and alsamixer), where the increase in volume is linear to the ear
; Default: false
mapped = true

; Available tags:
;   <label-volume> (default)
;   <ramp-volume>
;   <bar-volume>
format-volume = <ramp-volume> <label-volume>

; Available tags:
;   <label-muted> (default)
;   <ramp-volume>
;   <bar-volume>
;format-muted = <label-muted>

; Available tokens:
;   %percentage% (default)
;label-volume = %percentage%%

; Available tokens:
;   %percentage% (default)
label-muted = 🔇 muted
label-muted-foreground = #66

; Only applies if <ramp-volume> is used
ramp-volume-0 = 🔈
ramp-volume-1 = 🔉
ramp-volume-2 = 🔊

; If defined, it will replace <ramp-volume> when
; headphones are plugged in to `headphone_control_numid`
; If undefined, <ramp-volume> will be used for both
; Only applies if <ramp-volume> is used
ramp-headphones-0 = 
ramp-headphones-1 = 

[module/pulseaudio-tail]
type = custom/script
exec = ~/polybar-scripts/pulseaudio-tail.sh
tail = true
click-right = exec pavucontrol &
click-left = ~/polybar-scripts/pulseaudio-tail.sh --mute
scroll-up = ~/polybar-scripts/pulseaudio-tail.sh --up
scroll-down = ~/polybar-scripts/pulseaudio-tail.sh --down

[module/battery-combined-udev]
type = custom/script
exec = ~/polybar-scripts/battery-combined-udev.sh
tail = true

[module/updates-arch-combined]
type = custom/script
exec = ~/polybar-scripts/updates-arch-combined.sh
interval = 600

[module/player-mpris-tail]
type = custom/script
exec = ~/polybar-scripts/player-mpris-tail.py
tail = true
click-left = ~/polybar-scripts/player-ctrl.sh previous
click-right = ~/polybar-scripts/player-ctrl.sh next
click-middle = ~/polybar-scripts/player-ctrl.sh play-pause

[module/networkspeedup]
type = internal/network
interface = wlp2s0
label-connected = "%upspeed:7%"
format-connected = <label-connected>
format-connected-prefix = " "
format-connected-prefix-foreground = #c5c8c6

[module/networkspeedupeth]
type = internal/network
interface = enp3s0
label-connected = "%upspeed:7%"
format-connected = <label-connected>
format-connected-prefix = " "
format-connected-prefix-foreground = #c5c8c6

[module/info-wmname]
type = custom/script
exec = /home/lalantha/polybar-scripts/info-wmname.sh
label = " i3WM"
interval = 5
click-left = ~/polybar-scripts/info-wmname.sh --toggle