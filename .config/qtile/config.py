from libqtile.config import Key, Screen, Group, Drag, Click, hook
from libqtile.command import lazy
from libqtile.config import EzKey
from libqtile import layout, bar, widget
from plasma import Plasma
import subprocess


# Startup Scripts
def execute_once(process):
    return subprocess.Popen(process.split())

@hook.subscribe.startup
def startup():
    execute_once('feh --bg-scale /home/lalantha/Pictures/ghost-in-the-shell-3-1366×768.jpg')
    execute_once('/usr/bin/nm-applet')
   # execute_once('/home/lalantha/.config/polybar/launch.sh')
    execute_once('/usr/bin/blueman-applet')
    execute_once('/usr/bin/dropbox')
    execute_once('/usr/bin/utox')
    execute_once('/usr/bin/dunst')
    execute_once('/usr/bin/light-locker')
    execute_once('/home/jv/Develop/bin/screentemp')

mod = "mod4"

keys = [
    # Switch between windows in current stack pane
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),

    # Switch window focus to other pane(s) of stack
    Key([mod], "Right", lazy.layout.next()),
    Key([mod], "Left", lazy.layout.previous()),

    # Swap panes of split stack
    Key([mod, "shift"], "r", lazy.layout.rotate()),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),
    # Key([mod], "t", lazy.spawn("urxvt")),

    # Toggle between different layouts as defined below
    Key([mod], "Return", lazy.spawn("urxvt")),
    Key([mod], "q", lazy.window.kill()),

    # Misc keybindings
    Key([mod], "h", lazy.layout.mode_horizontal()),
    Key([mod], "v", lazy.layout.mode_vertical()),
    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),
    Key([mod], "r", lazy.spawncmd()),
    Key([mod], "d", lazy.spawn("rofi -show run")),
    Key([mod], "o", lazy.spawn("google-chrome-stable")),
    Key([mod], "p", lazy.spawn("thunar")),
    Key([mod], "l", lazy.spawn("light-locker-command --lock")),
    # Key([mod], "r", lazy.spawn("/home/jv/Develop/bin/twitch")),
    Key([], "Print", lazy.spawn("xfce4-screenshooter")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q set 'Master' 10%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q set 'Master' 10%+")),
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set 'Master' toggle")),
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 10%")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 10%")),
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod, "control"], "e", lazy.spawn("/home/lalantha/.config/qtile/shutdown_menu")),
]

groups = [Group(i) for i in "1234567"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen()),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
    ])

layouts = [
    layout.Max(),
    layout.MonadTall(),
    #layout.Stack(num_stacks=1, border_width=0, margin=1),
    #layout.Stack(num_stacks=2, border_width=3, margin=1),
    Plasma(
        border_normal='#333333',
        border_focus='#00e891',
        border_normal_fixed='#006863',
        border_focus_fixed='#00e8dc',
        border_width=1,
        border_width_single=0,
        margin=0 )
]

widget_defaults = dict(
    font='DejaVuSansMonoForPowerline Nerd Font',
    fontsize=13,
    padding=3,
)
default_configuration1 = dict(
    fontsize=12,
    foreground="FFFFFF",
    background=["34353E", "34353E"],
    font="ttf-droid",
    margin_y=2,
    font_shadow="000000",
)
default_configuration2 = dict(
    fontsize=12,
    foreground="FFFFFF",
    background=["34353E", "34353E"],
    font="ttf-droid",
    margin_y=2,
    font_shadow="000000",
)
group_configuration = dict(
    active="FFFFFF",
    inactive="FFFFFF",
    fontsize=12,
    background=["34353E", "34353E"],
    font="ttf-droid",
    margin_y=2,
    margin_x=2,
    font_shadow="000000")

extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [

            widget.TextBox(text=" ", **default_configuration1),
              # widget.TextBox(text="◤ ", fontsize=60, padding=-5, foreground=["232729", "292A2A"], background=["34353E", "34353E"]),
               widget.GroupBox(**group_configuration),
               widget.TextBox(text="         ", **default_configuration1),
                widget.WindowName(**default_configuration2),
                #widget.TextBox(text="◤ ", fontsize=60, padding=-5, foreground=["232729", "292A2A"], background=["34353E", "34353E"]),
                #widget.TextBox(text="CPU", **default_configuration1),
               # widget.CPUGraph(core="all", frequency=5, line_width=2, border_width=1, background=["33393B", "232729"]),
                #widget.TextBox(text="  MEM", **default_configuration1),
                #widget.Memory(fmt="{MemUsed}M", update_interval=10, background=["33393B", "232729"]),
                #widget.TextBox(text="  BAT", **default_configuration1),
                #widget.Battery(battery_name="BAT0", charge_char="▲", discharge_char="▼", format="{percent:2.0%} {char}", background=["33393B", "232729"]),
		
widget.TextBox(text=" Qtile  ", **default_configuration1),
                widget.TextBox(text="  ", **default_configuration1),
                widget.Volume(channel="Master", background=["34353E", "34353E"]),
                widget.TextBox(text="  ", **default_configuration1),
                #widget.TextBox(text="◤ ", fontsize=60, padding=-5, foreground=["33393B", "232729"], background=["34353E", "34353E"]),
                 widget.TextBox(text=" ", **default_configuration1),
                widget.Clock(**default_configuration2, format='%H:%M - %a,%d.%m.%Y'),
                #widget.TextBox(text="◤ ", fontsize=60, padding=-5, foreground=["232729", "292A2A"], background=["34353E", "34353E"]),
                 widget.TextBox(text="  ", **default_configuration1),
                widget.Systray(**default_configuration1),
            ],
            22,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

wmname = "Qtile"