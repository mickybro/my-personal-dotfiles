colorscheme gruvbox
set background=dark
set termguicolors
let g:gruvbox_contrast_dark = 'hard'
let g:gruvbox_italic=1
set number